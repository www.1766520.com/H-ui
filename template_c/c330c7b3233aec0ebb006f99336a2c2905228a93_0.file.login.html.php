<?php
/* Smarty version 3.1.32, created on 2018-07-31 09:00:42
  from '/home/www/nettest/template/admht/login.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b60253a77e952_64012917',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c330c7b3233aec0ebb006f99336a2c2905228a93' => 
    array (
      0 => '/home/www/nettest/template/admht/login.html',
      1 => 1533027628,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b60253a77e952_64012917 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link href="/static/admht/h-ui/css/H-ui.min.css" rel="stylesheet" type="text/css" />
<link href="/static/admht/h-ui/css/H-ui.login.css" rel="stylesheet" type="text/css" />
<link href="/static/admht/h-ui.admin/css/style.css" rel="stylesheet" type="text/css" />
<link href="/static/admht/lib/Hui-iconfont/1.0.8/iconfont.css" rel="stylesheet" type="text/css" />
<title>后台登录</title>
</head>
<body>
<input type="hidden" id="TenantId" name="TenantId" value="" />
<div class="header"></div>
<div class="loginWraper">
	<div id="loginform" class="loginBox">
		<form class="form form-horizontal" action="admht.php?tp=<?php echo $_smarty_tpl->tpl_vars['tp']->value;?>
" method="post">
			<div class="row cl">
				<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60d;</i></label>
				<div class="formControls col-xs-8">
					<input id="" name="username" type="text" placeholder="账户" class="input-text size-L">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60e;</i></label>
				<div class="formControls col-xs-8">
					<input id="" name="passwd" type="password" placeholder="密码" class="input-text size-L">
				</div>
			</div>
			<div class="row cl">
				<div class="formControls col-xs-8 col-xs-offset-3">
					<input name="" type="submit" class="btn btn-success radius size-L" value="&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;">
					<input name="" type="reset" class="btn btn-default radius size-L" value="&nbsp;取&nbsp;&nbsp;&nbsp;&nbsp;消&nbsp;">
                    <input type="hidden" name="op" value="login"/>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="footer">Copyright </div>
</body>
</html><?php }
}
