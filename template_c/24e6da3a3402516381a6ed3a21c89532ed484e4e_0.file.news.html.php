<?php
/* Smarty version 3.1.32, created on 2018-08-14 15:23:34
  from 'D:\laragon\www\H-ui\template\admht\news.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b72837693feb9_35495993',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '24e6da3a3402516381a6ed3a21c89532ed484e4e' => 
    array (
      0 => 'D:\\laragon\\www\\H-ui\\template\\admht\\news.html',
      1 => 1534231411,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admht/_meta.html' => 1,
    'file:admht/_header.html' => 1,
    'file:admht/_menu.html' => 1,
    'file:admht/_footer.html' => 1,
  ),
),false)) {
function content_5b72837693feb9_35495993 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'D:\\laragon\\www\\H-ui\\source\\smarty\\plugins\\modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
$_smarty_tpl->_subTemplateRender("file:admht/_meta.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender("file:admht/_header.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender("file:admht/_menu.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<section class="Hui-article-box">
	<nav class="breadcrumb"><i class="Hui-iconfont"></i> <a href="/admht.php" class="maincolor">首页</a> 
		<?php echo $_smarty_tpl->tpl_vars['com']->value['crumb'];?>

		<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
	</nav>
	<div class="Hui-article">
		<article class="cl pd-20">
		<?php if ($_smarty_tpl->tpl_vars['op']->value == 'list') {?>
			<div class="codeView docs-example">
				<div class="row cl">
					<div class="col-xs-6 col-sm-2">
						<select class="select" size="1" name="demo1">
							<option value="" selected>--选择所属板块--</option>
							<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['column_list']->value, 'v');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['v']->value) {
?>
							<option value="<?php echo $_smarty_tpl->tpl_vars['v']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['v']->value['name'];?>
</option>
							<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
						</select>
					</div>
					<div class="col-xs-6 col-sm-2">
						<input class="btn btn-primary radius" type="button" value="新 增" onclick="window.location.href='/admht.php?tp=news&op=show_add'">
					</div>
				</div>
				<p><br/></p>
				<table class="table table-border table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th>ID</th>
							<th>标题</th><th>标题</th><th>标题</th>
							<th>所属板块</th>
							<th>封面</th>
							<th>首页</th>
							<th>排序</th>
							<th>阅读总数</th>
							<th>评论总数</th>
							<th>编辑人</th>
							<th>审核状态</th>
							<th>添加时间</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
						<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['list']->value, 'val');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['val']->value) {
?>
						<tr>
							<td><?php echo $_smarty_tpl->tpl_vars['val']->value['id'];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['val']->value['title'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['val']->value['title'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['val']->value['title'];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['show_list']->value[$_smarty_tpl->tpl_vars['val']->value['news_sort']];?>
</td>
							<td><?php if ($_smarty_tpl->tpl_vars['val']->value['cover']) {?><i class="Hui-iconfont" style="color:#5eb95e;">&#xe6a7;</i><?php } else { ?><i class="Hui-iconfont" style="color:#f00;">&#xe6a6;</i><?php }?></td>
							<td><?php if ($_smarty_tpl->tpl_vars['val']->value['is_ui']) {?>
	                        <i class="Hui-iconfont" style="color:#5eb95e;">&#xe6a7;</i></i>
	                        <?php } else { ?>
	                        <i class="Hui-iconfont" style="color:#f00;">&#xe6a6;</i></i>
	                        <?php }?></td>
							<td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['val']->value['sortby'];?>
" name="sortby" style="width:40px;margin:0 auto;"/></td>
							<td><?php echo $_smarty_tpl->tpl_vars['val']->value['click_sum'];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['val']->value['reward_sum'];?>
</td>
							<td><?php echo $_smarty_tpl->tpl_vars['val']->value['author_name'];?>
</td>
							<td><?php if ($_smarty_tpl->tpl_vars['val']->value['is_audit'] == 0) {?>未审核
	                        <?php } elseif ($_smarty_tpl->tpl_vars['val']->value['is_audit'] == 1) {?>通过
	                        <?php } elseif ($_smarty_tpl->tpl_vars['val']->value['is_audit'] == 2) {?>驳回
	                        <?php }?></td>
							<td><?php if ($_smarty_tpl->tpl_vars['val']->value['audit_time']) {?>
	                        <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['val']->value['audit_time'],"Y-m-d H:i:s");?>

	                        <?php } else { ?>
	                        <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['val']->value['create_time'],"Y-m-d H:i:s");?>

	                        <?php }?></td>
							<td>表格内容</td>
						</tr>
						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</tbody>
				</table>
				<?php echo $_smarty_tpl->tpl_vars['multi_url']->value;?>

			</div>
		<?php }?>
		
		<?php if ($_smarty_tpl->tpl_vars['op']->value == 'show_add') {?>
			<div class="codeView docs-example">
				<form method="post" class="form form-horizontal" id="demoform-2" novalidate="novalidate">
					<legend>带验证表单</legend>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">邮箱：</label>
						<div class="formControls col-xs-8 col-sm-9">
							<input type="text" class="input-text" placeholder="@" name="email" id="email">
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">用户名：</label>
						<div class="formControls col-xs-8 col-sm-9">
							<input type="text" class="input-text" placeholder="4~16个字符，字母/中文/数字/下划线" name="username" id="username">
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">账户：</label>
						<div class="formControls col-xs-8 col-sm-9">
							<input type="text" class="input-text" autocomplete="off" placeholder="手机/邮箱" name="account" id="account">
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">密码：</label>
						<div class="formControls col-xs-8 col-sm-9">
							<input type="password" class="input-text" autocomplete="off" placeholder="密码" name="password" id="password">
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">密码验证：</label>
						<div class="formControls col-xs-8 col-sm-9">
							<input type="password" class="input-text" autocomplete="off" placeholder="密码" name="password2" id="password2">
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">网址：</label>
						<div class="formControls col-xs-8 col-sm-9">
							<input type="text" class="input-text" autocomplete="off" placeholder="http://" name="url" id="url">
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">单选框：</label>
						<div class="formControls skin-minimal col-xs-8 col-sm-9">
							<div class="radio-box">
								<input type="radio" id="sex-1" name="sex" value="1">
								<label for="sex-1" class="">男</label>
							</div>
							<div class="radio-box">
								<input type="radio" id="sex-2" name="sex" value="2">
								<label for="sex-2" class="">女</label>
							</div>
							<div class="radio-box">
								<input type="radio" id="sex-3" name="sex" value="0">
								<label for="sex-3" class="">保密</label>
							</div>
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">爱好：</label>
						<div class="formControls skin-minimal col-xs-8 col-sm-9">
							<div class="check-box">
								<input type="checkbox" id="aihao-5" name="aihao[]" value="1">
								<label for="checkbox-5" class="">上网</label>
							</div>
							<div class="check-box">
								<input type="checkbox" id="aihao-6" name="aihao[]" value="2">
								<label for="checkbox-6">摄影</label>
							</div>
						</div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">附件：</label>
						<div class="formControls col-xs-8 col-sm-9"> <span class="btn-upload form-group">
							<input class="input-text upload-url" type="text" name="uploadfile1" id="uploadfile1" readonly="" style="width:200px">
							<a href="javascript:void();" class="btn btn-primary upload-btn"><i class="Hui-iconfont"></i> 浏览文件</a>
							<input type="file" multiple="" name="file-2" class="input-file">
							</span> </div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">所在城市：</label>
						<div class="formControls col-xs-8 col-sm-9"> <span class="select-box">
							<select class="select" size="1" name="city">
								<option value="" selected="">请选择城市</option>
								<option value="1">北京</option>
								<option value="2">上海</option>
								<option value="3">广州</option>
							</select>
							</span> </div>
					</div>
					<div class="row cl">
						<label class="form-label col-xs-4 col-sm-3">备注：</label>
						<div class="formControls col-xs-8 col-sm-9">
							<textarea class="textarea" placeholder="说点什么...最少输入10个字符" name="beizhu"></textarea>
							<p class="textarea-numberbar"><em class="textarea-length">0</em>/500</p>
						</div>
					</div>
					<div class="row cl">
						<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
							<input class="btn btn-primary" type="submit" id="btn-on" value="&nbsp;&nbsp;提 交&nbsp;&nbsp;">
						</div>
					</div>
				</form>
			</div>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['op']->value == 'edit') {?>
		
		<?php }?>



		</article>	
	</div>
</section>
<!--/请在上方写此页面业务相关的脚本-->
<?php echo '<script'; ?>
 type="text/javascript">
$(function(){
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});

	$("#demoform-2").validate({
		rules:{
			email:{
				required:true,
				email:true,
			},
			username:{
				required:true,
				minlength:4,
				maxlength:16
			},
			account:{
				required:true,
			},
			password:{
				required:true,
			},
			password2:{
				required:true,
				equalTo: "#password"
			},
			url:{
				required:true,
				url:true,
			},
			sex:{
				required:true,
			},
			aihao:{
				required:true,
			},
			fujian:{
				required:true,
			},
			city:{
				required:true,
			},
			url:{
				required:true,
				url:true
			},
			beizhu:{
				required:true,
			},
		},
		onkeyup:false,
		focusCleanup:true,
		success:"valid",
		submitHandler:function(form){
			$(form).ajaxSubmit({
				url:'/admht.php?tp=news&op=do_add',
				type:'post',
				dataType:'json',
				beforeSend: function(){
			    	$('#btn-on').addClass('disabled');
			    },
				success:function(json){
					if (json.status ==1) {
						layer.msg(json.msg);
						setTimeout("window.location.href='"+json.data.url+"'",1000);
					}else{
						layer.msg(json.msg);
					}
					$('#btn-on').removeClass('disabled');
				},
				complete: function(){
					
				}
			});
		}
	});
});
<?php echo '</script'; ?>
>
<!--_footer 作为公共模版分离出去-->
<?php $_smarty_tpl->_subTemplateRender("file:admht/_footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
