<?php
/* Smarty version 3.1.32, created on 2018-08-13 16:40:13
  from 'D:\laragon\www\H-ui\template\admht\login.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b7143eda59bf2_27924240',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cb55586a9db633cd2086281c1cc69b9e5065efb4' => 
    array (
      0 => 'D:\\laragon\\www\\H-ui\\template\\admht\\login.html',
      1 => 1534149608,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admht/_footer.html' => 1,
  ),
),false)) {
function content_5b7143eda59bf2_27924240 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<LINK rel="Bookmark" href="/static/admht/assets/favicon.ico" >
<LINK rel="Shortcut Icon" href="/static/admht/assets/favicon.ico" />
<!--[if lt IE 9]>
<?php echo '<script'; ?>
 type="text/javascript" src="lib/html5.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="lib/respond.min.js"><?php echo '</script'; ?>
>
<![endif]-->
<link rel="stylesheet" type="text/css" href="/static/admht/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="/static/admht/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/static/admht/lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="/static/admht/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="/static/admht/h-ui.admin/css/H-ui.login.css" />
<link rel="stylesheet" type="text/css" href="/static/admht/h-ui.admin/css/style.css" />
<!--[if IE 6]>
<?php echo '<script'; ?>
 type="text/javascript" src="http://lib.h-ui.net/DD_belatedPNG_0.0.8a-min.js" ><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>DD_belatedPNG.fix('*');<?php echo '</script'; ?>
><![endif]-->
<?php echo '<script'; ?>
 type="text/javascript" src="/static/admht/lib/jquery/1.9.1/jquery.min.js"><?php echo '</script'; ?>
> 
<title>系统登录--天天视频</title>
</head>
<body>
<input type="hidden" id="TenantId" name="TenantId" value="" />
<div class="header"></div>
<div class="loginWraper">
	<div id="loginform" class="loginBox">
		<form class="form form-horizontal" id="frm" method="post">
			<div class="row cl">
				<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60d;</i></label>
				<div class="formControls col-xs-8">
					<input name="username" type="text" placeholder="账户" class="input-text size-L">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60e;</i></label>
				<div class="formControls col-xs-8">
					<input name="passwd" type="password" placeholder="密码" class="input-text size-L">
				</div>
			</div>
			<div class="row cl" style="display: none;">
				<label class="form-label col-xs-3"></label>
				<div class="formControls col-xs-8">
					<span class="form-point"></span>
				</div>
			</div>
			<div class="row cl">
				<div class="formControls col-xs-8 col-xs-offset-3">
					<input name="" type="button" class="btn btn-success radius size-L" value="登 录">
					<input name="" type="reset" class="btn btn-default radius size-L" value="取 消">
				</div>
			</div>
		</form>
	</div>
</div>
<div class="footer">Copyright 2018</div>
<?php echo '<script'; ?>
 type="text/javascript">
	$(function(){
		$('.btn-success').click(function(){
			var data =new FormData(document.getElementById("frm"));
			$.ajax({
				url:'admht.php?tp=login&op=check',
				type:'post',
				data:data,
				dataType:'json',
				cache: false,
			    processData: false,
			    contentType: false,
			    beforeSend: function(){
			    	$('.btn-success').addClass('disabled');
			    },
				success:function(json){
					if (json.status ==1) {
						layer.msg(json.msg);
						//$.Huimodalalert(json.msg,1000);
						setTimeout("window.location.href='"+json.data.url+"'",1000);
					}else{
						layer.msg(json.msg);
						//$.Huimodalalert(json.msg,1000);
					}
					$('.btn-success').removeClass('disabled');
				},
				complete: function(){

				}
			});
		});
	});
<?php echo '</script'; ?>
>
<?php $_smarty_tpl->_subTemplateRender("file:admht/_footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
