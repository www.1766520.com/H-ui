<?php
/* Smarty version 3.1.32, created on 2018-08-10 07:26:55
  from 'D:\laragon\www\nettest\template\admht\login.html' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32',
  'unifunc' => 'content_5b6d3e3fde7159_44784220',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '54988ad68740f4d56594a2f6f318e0c27025c7ac' => 
    array (
      0 => 'D:\\laragon\\www\\nettest\\template\\admht\\login.html',
      1 => 1533886014,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admht/_meta.html' => 1,
    'file:admht/_footer.html' => 1,
  ),
),false)) {
function content_5b6d3e3fde7159_44784220 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender("file:admht/_meta.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
<body>
<input type="hidden" id="TenantId" name="TenantId" value="" />
<div class="header"></div>
<div class="loginWraper">
	<div id="loginform" class="loginBox">
		<form class="form form-horizontal" id="frm" method="post">
			<div class="row cl">
				<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60d;</i></label>
				<div class="formControls col-xs-8">
					<input name="username" type="text" placeholder="账户" class="input-text size-L">
				</div>
			</div>
			<div class="row cl">
				<label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60e;</i></label>
				<div class="formControls col-xs-8">
					<input name="passwd" type="password" placeholder="密码" class="input-text size-L">
				</div>
			</div>
			<div class="row cl" style="display: none;">
				<label class="form-label col-xs-3"></label>
				<div class="formControls col-xs-8">
					<span class="form-point"></span>
				</div>
			</div>
			<div class="row cl">
				<div class="formControls col-xs-8 col-xs-offset-3">
					<input name="" type="button" class="btn btn-success radius size-L" value="登 录">
					<input name="" type="reset" class="btn btn-default radius size-L" value="取 消">
				</div>
			</div>
		</form>
	</div>
</div>
<div class="footer">Copyright </div>
<?php echo '<script'; ?>
 type="text/javascript">
	$(function(){
		$('.btn-success').click(function(){
			var data =new FormData(document.getElementById("frm"));
			$.ajax({
				url:'admht.php?tp=login&op=check',
				type:'post',
				data:data,
				dataType:'json',
				cache: false,
			    processData: false,
			    contentType: false,
				success:function(json){
					if (json.status ==1) {
						$.Huimodalalert(json.msg,1000);
						setTimeout("window.location.href='"+json.data.url+"'",1000);
					}else{
						console.log(json.msg);
						$.Huimodalalert(json.msg,1000);
					}
				}
			});
		});
	});
<?php echo '</script'; ?>
>
<?php $_smarty_tpl->_subTemplateRender("file:admht/_footer.html", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
</body>
</html><?php }
}
