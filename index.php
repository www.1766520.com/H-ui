<?php
define('S_ROOT', dirname(__FILE__).DIRECTORY_SEPARATOR);
require_once("source/config.php");
require_once("source/common.php");
require_once("source/db.php");

if(!isset($_GET['tp']) || empty($_GET['tp']) )
{
	echo 'welcome';
	exit;
}
else
 {
 	$tp=$_GET["tp"];
	argsFilter();
	$tpl=init_smarty();
	include_once("source/{$tp}.php");
}