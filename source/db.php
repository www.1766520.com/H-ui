<?php
class Mysqloper
{
	protected $dblink;
	private static $instance;

	function __construct()
	{	
		global $dbconfig;
		$this->dblink = new mysqli($dbconfig["host"],$dbconfig["user"],$dbconfig["pwd"],$dbconfig["name"]);
		//$this->dblink = mysqli_connect($dbconfig["host"], $dbconfig["user"], $dbconfig["pwd"], $dbconfig["name"]);
		$this->dblink->query("set names utf8mb4");
	}

	public static function getinstance(){
        if(!(self::$instance instanceof self)){
            self::$instance = new self();
        }
        return self::$instance;
    }
    // 查询一次
	public function exec_db($sql)
	{
		$result=$this->dblink->query($sql);
		return $result;
	}
	// 获取一行数据
	public function getrecord($sql)
	{
		$result =$this->exec_db($sql);
		$row = $result->fetch_assoc();
		$result->free();
		return $row;
	}
	// 获取新增id
	public function exec_db_insertid($sql) {
        $result = $this->exec_db($sql);
        return $this->dblink->insert_id;
    }

	function __destruct()
    {
        $this->dblink->close();
    }
}

class Memoper
{
	protected $memcache_obj;

	function __construct()
	{
		global $memconfig;
		$this->memcache_obj = memcache_connect($memconfig["host"],$memconfig["port"]);
	}

	public function mem_set($key,$value,$time=0)
	{
		memcache_set($this->memcache_obj,$key,$value,false,$time);		
	}

	public function mem_get($key)
	{
		return memcache_get($this->memcache_obj,$key);		
	}
}
?>