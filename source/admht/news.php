<?php
if (isset($_GET['op']) && $_GET['op'] =='list') {
	// 新闻分类
	$column_list = getNewsColumn();
	$show_list =array();
	foreach ($column_list as $k => $v) {
		if ($v['gradation']==1) {
			$column_list[$k]['name'] =$v['name'];
		}elseif ($v['gradation']==2) {
			$column_list[$k]['name'] ='|--'.$v['name'];
			$show_list[$column_list[$k]['id']] =$v['name'];  // 新闻列表所属板块集合
		}elseif ($v['gradation']==3){
			$column_list[$k]['name'] ='|--|--'.$v['name'];
		}elseif ($v['gradation']==4){
			$column_list[$k]['name'] ='|--|--|--'.$v['name'];
		}elseif($v['gradation']>4){
			unset($row[$k]);
		}
	}
    $tpl->assign('column_list',$column_list);
    $tpl->assign('show_list',$show_list);

	$list =array();
	$query = exec_db("SELECT * FROM tab_news ORDER BY id DESC LIMIT {$start},{$perpage}");
	while ($row = mysqli_fetch_assoc($query)) {
		$list[] =$row;
	}

	$count =getrecord('SELECT count(*) AS count FROM tab_news')['count'];
	$multi_url =multi_url($count,$perpage,$curpage,$mpurl);
	//print_r($list);die;
	$tpl->assign('list',$list);
	$tpl->assign('multi_url',$multi_url);

}
if (isset($_GET['op']) && $_GET['op'] =='show_add') {
	# code...
}
if (isset($_GET['op']) && $_GET['op'] =='do_add') {
	//print_r($_POST);die;
	echojson('添加成功', 1, array('url'=>'/admht.php?tp=news&op=list'));
}
if (isset($_GET['op']) && $_GET['op'] =='del') {
	# code...
}
$tpl->display("admht/{$tp}.html");



/*
 * 无限极分类,数组形式
 */
function getNewsColumn($pid=0,$num=0){
	static $arr =array();
	$sql ='SELECT id,name,pid,channel_id,sort,gid,status FROM tab_news_column WHERE pid='.$pid.' AND channel_id=1 ORDER BY sort DESC,id ASC';
	$query =exec_db($sql);
	$list =array();
	while ($row = mysqli_fetch_assoc($query)) {
		$list[] =$row;
	}
	$num++;
	if ($list) {
		foreach ($list as $k => $v) {
			$list[$k]['gradation'] =$num;
			$arr[] =$list[$k];
			getNewsColumn($v['id'],$num);
		}
	}
	return $arr;
}
