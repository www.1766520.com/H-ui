<?php
date_default_timezone_set("Asia/Shanghai");
define('S_ROOT', dirname(__FILE__).DIRECTORY_SEPARATOR);
require_once("source/config.php");
require_once("source/common.php");

argsFilter();
session_start();

$user_info =isset($_SESSION['user_info'])?$_SESSION['user_info']:'';
if(!isset($_GET['tp']) || empty($_GET['tp']) )
{	
	header('Location:admht.php?tp=index');
	die;
}else{
	if (empty($user_info) && $_GET['tp'] !='login') {
		header('Location:admht.php?tp=login');
		die;
	}
	elseif ($_GET['tp'] =='login' && $_GET['op'] !='check') {
		session_destroy();
		$user_info ='';
	}
}

$tp =$_GET['tp'];
$op =isset($_GET['op'])?$_GET['op']:'';

$tpl=init_smarty();
$tpl->assign('tp', $tp);
$tpl->assign('op', $op);
// 获取左侧列表
if ($user_info) {
	$com =getColumn($user_info['gid'], $user_info['id'], $tp, $op);
	$tpl->assign('user_info', $user_info);
	$tpl->assign('com', $com);

	// 分页
	$curpage = isset($_GET['page'])?$_GET['page']:1;
	$perpage = 15;
	$start   = ($curpage-1)*$perpage;
	$mpurl   = $_SERVER['REQUEST_URI'];
	$prev_mpurl = $_SERVER['HTTP_REFERER'];

}

include_once("source/admht/{$tp}.php");
?>